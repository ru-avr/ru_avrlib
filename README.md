# ru_avrlib

## How to add to project

Adding submodule to project:

```bash
mkdir -p libs && cd libs
git submodule add https://gitlab.com/ru-avr/ru_avrlib.git
# or
git submodule add git@gitlab.com:ru-avr/ru_avrlib.git
```

and add modules to your makefile

```Makefile
include ./libs/ru_avrlib/ru_avrlib.mk

INCDIRS += ./libs/
C_SOURCES += libs/ru_avrlib/atmega/usart.c  # example
```
