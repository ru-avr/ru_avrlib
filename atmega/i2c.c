/*
 *  i2c.c
 *
 *  Created on: 2019-11-21
 *  Author: rafau
 */

#include "i2c.h"

#include <avr/io.h>

uint8_t i2c_init(i2c_scl_freq freq) {
    // f_scl = F_CPU / (16 + 2 * TWBR * prescaler)
    // prescaler - TWPS1 and TWPS0 in TWBR
    // TWBR = (F_CPU - 16 * f_scl) / (2 * prescaler * f_scl)

    // TODO: fill rest clk parameters
    switch (freq) {
        case i2c_scl_freq_50kHz:

#if F_CPU == 1000000
            TWBR = 0x01;
            TWSR = 0x00;
#elif F_CPU == 16000000
            TWBR = 0x46;
            TWSR = 0x00;
#endif

            break;
        case i2c_scl_freq_100kHz:

#if F_CPU == 16000000
            TWBR = 0x46;
            TWSR = 0x00;
#endif

            break;
        case i2c_scl_freq_400kHz: break;

        default: return 1;
    }

    TWCR = (1 << TWEN);

    return 0;
}

void i2c_start(void) {
#if defined(__AVR_ATmega168PA__) || defined(__AVR_ATmega328P__)
    TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
    while (!(TWCR & (1 << TWINT))) {}
#endif
}

void i2c_stop(void) {
#if defined(__AVR_ATmega168PA__) || defined(__AVR_ATmega328P__)
    TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN);
#endif
}

void i2c_write(uint8_t byte) {
#if defined(__AVR_ATmega168PA__) || defined(__AVR_ATmega328P__)
    TWDR = byte;
    TWCR = (1 << TWINT) | (1 << TWEN);
    while (!(TWCR & (1 << TWINT))) {}
#endif
}

uint8_t i2c_read_ACK(void) {
#if defined(__AVR_ATmega168PA__) || defined(__AVR_ATmega328P__)
    TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);
    while (!(TWCR & (1 << TWINT))) {}
    return TWDR;
#endif
}

uint8_t i2c_read_NACK(void) {
#if defined(__AVR_ATmega168PA__) || defined(__AVR_ATmega328P__)
    TWCR = (1 << TWINT) | (1 << TWEN);
    while (!(TWCR & (1 << TWINT))) {}
    return TWDR;
#endif
}

uint8_t i2c_get_status(void) {
#if defined(__AVR_ATmega168PA__) || defined(__AVR_ATmega328P__)
    return (TWSR & 0b11111000);
#endif
}
