/*
 *  usart.h
 *
 *  Created on: 2020-02-09
 *  Author: rafau
 */

#ifndef __RU_AVRLIB_ATMEGA_USART_H__
#define __RU_AVRLIB_ATMEGA_USART_H__

#include <stdarg.h>
#include <stdint.h>

#ifndef USART_PRINTF_BUFFER_SIZE
#pragma message("USART_PRINTF_BUFFER_SIZE not defined, default value used. USART_PRINTF_BUFFER_SIZE=256")
#define USART_PRINTF_BUFFER_SIZE 256
#endif  // !USART_PRINTF_BUFFER_SIZE

/**
 * Initialize usart, no parity, 8 data bits, 1 stop bit
 */
uint8_t usart_init(uint32_t baud);

/**
 * Blocking, send byte to uart
 */
void usart_putc(char c);

/**
 * Blocking, receive byte from uart
 */
char usart_getc(void);

/**
 * Blocking, send string to uart
 */
void usart_puts(char *s);

/**
 * Printf to uart
 */
int usart_printf(const char *format, ...);

#endif /* !__RU_AVRLIB_ATMEGA_USART_H__ */
