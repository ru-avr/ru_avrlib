/*
 *  i2c.h
 *
 *  Created on: 2019-11-21
 *  Author: rafau
 */

#ifndef __RU_AVRLIB_ATMEGA_I2C_H__
#define __RU_AVRLIB_ATMEGA_I2C_H__

#include <stdint.h>
#include <stdlib.h>

typedef enum i2c_scl_freq {
    i2c_scl_freq_50kHz,  //
    i2c_scl_freq_100kHz,
    i2c_scl_freq_400kHz
} i2c_scl_freq;

uint8_t i2c_init(i2c_scl_freq freq);

void i2c_start(void);

void i2c_stop(void);

void i2c_write(uint8_t byte);

uint8_t i2c_read_ACK(void);

uint8_t i2c_read_NACK(void);

#endif /* !__RU_AVRLIB_ATMEGA_I2C_H__ */
