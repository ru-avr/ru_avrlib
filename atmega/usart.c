/*
 *  usart.c
 *
 *  Created on: 2020-02-09
 *  Author: rafau
 */

#include "usart.h"

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef F_CPU
#error "F_CPU not defined"
#endif  // !F_CPU

uint8_t usart_init(uint32_t baud) {
#if defined(__AVR_ATmega168PA__) || defined(__AVR_ATmega328P__)
    uint32_t ubrr = ((F_CPU / 16) / baud) - 1;
    UBRR0H = (uint8_t)(ubrr >> 8) & 0xFF;
    UBRR0L = (uint8_t)ubrr & 0xFF;

    // data bits: 8
    // stop bits: 1
    // parity: none
    UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);

    // turn on transmitter and receiver
    UCSR0B = (1 << TXEN0) | (1 << RXEN0);
    return 0;
#else
    return 1;
#endif
}

void usart_putc(char c) {
#if defined(__AVR_ATmega168PA__) || defined(__AVR_ATmega328P__)
    while (!(UCSR0A & (1 << UDRE0))) {
    }
    UDR0 = c;
#endif
}

char usart_getc(void) {
#if defined(__AVR_ATmega168PA__) || defined(__AVR_ATmega328P__)
    while (!(UCSR0A & (1 << RXC0))) {
    }
    return UDR0;
#endif
}

void usart_puts(char *s) {
    while (*s)
        usart_putc(*s++);
}

int usart_printf(const char *format, ...) {
    static char buffer[255] = {0};

    va_list args;
    int rc;

    va_start(args, format);
    rc = vsprintf(buffer, format, args);
    va_end(args);

    usart_puts(buffer);

    return rc;
}