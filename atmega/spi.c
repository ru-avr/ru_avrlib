/*
 *  spi.c
 *
 *  Created on: 2020-03-14
 *  Author: rafau
 */

#include "spi.h"

#include <avr/io.h>

uint8_t spi_init(spi_clock_divider_t clk_div, uint8_t clk_pol, uint8_t phase, uint8_t lsb) {
#if defined(__AVR_ATmega168PA__) || defined(__AVR_ATmega328P__)
    SPCR = 0x00;

    switch (clk_div) {
        case spi_clock_div_2: SPSR |= (1 << SPI2X); break;
        case spi_clock_div_4: SPSR &= ~(1 << SPI2X); break;
        case spi_clock_div_8:
            SPSR |= (1 << SPI2X);
            SPCR |= (1 << SPR0);
            break;
        case spi_clock_div_16:
            SPSR &= ~(1 << SPI2X);
            SPCR |= (1 << SPR0);
            break;
        case spi_clock_div_32:
            SPSR |= (1 << SPI2X);
            SPCR |= (1 << SPR1);
            break;
        case spi_clock_div_64:
            SPSR &= ~(1 << SPI2X);
            SPCR |= (1 << SPR1);
            break;
        case spi_clock_div_128:
            SPSR &= ~(1 << SPI2X);
            SPCR |= (1 << SPR1) | (1 << SPR0);
            break;
        default: return 2;
    }

    if (clk_pol) SPCR |= (1 << CPOL);
    if (phase) SPCR |= (1 << CPHA);
    if (lsb) SPCR |= (1 << DORD);
    SPCR |= (1 << MSTR) | (1 << SPE);

    return 0;
#else
    return 1;
#endif
}

uint8_t spi_transfer(uint8_t byte) {
#if defined(__AVR_ATmega168PA__) || defined(__AVR_ATmega328P__)
    SPDR = byte;
    while (!(SPSR & (1 << SPIF))) {}
    // byte = SPDR;
    // return byte;
    return SPDR;
#else
    return 0;
#endif
}