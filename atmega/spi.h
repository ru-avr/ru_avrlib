/*
 *  spi.h
 *
 *  Created on: 2020-03-14
 *  Author: rafau
 */

#ifndef __RU_AVRLIB_ATMEGA_SPI_H__
#define __RU_AVRLIB_ATMEGA_SPI_H__

#include <stdint.h>
#include <stdlib.h>

typedef enum spi_clock_divider {
    spi_clock_div_2,
    spi_clock_div_4,
    spi_clock_div_8,
    spi_clock_div_16,
    spi_clock_div_32,
    spi_clock_div_64,
    spi_clock_div_128,
} spi_clock_divider_t;

/**
 * Initialize spi.
 * SPI clock frequency is calculated from the formula:
 * spi_clock = freq/(2^(clk_div+1)) // 0 <= clk_div <= 7
 *
 * clk_div: parameter to SPI clock formula
 * clk_pol: clock polarity
 * phase:   clock phase
 * lsb:     select LSB mode
 */
uint8_t spi_init(spi_clock_divider_t clk_div, uint8_t clk_pol, uint8_t phase, uint8_t lsb);

uint8_t spi_transfer(uint8_t byte);

#endif /* !__RU_AVRLIB_ATMEGA_SPI_H__ */
