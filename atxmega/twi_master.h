/*
 *  twi_master.h
 *
 *  Created on: 2021-01-23
 *  Author: rafau
 */

#ifndef __RU_AVRLIB_ATXMEGA_TWI_MASTER_H__
#define __RU_AVRLIB_ATXMEGA_TWI_MASTER_H__

#include <avr/io.h>

#ifndef TWI_TRANSFER_MAX_LENGTH
#pragma message("TWI_TRANSFER_MAX_LENGTH not defined, default value used. TWI_TRANSFER_MAX_LENGTH=32")
#define TWI_TRANSFER_MAX_LENGTH 32
#endif  // !TWI_TRANSFER_MAX_LENGTH

typedef enum twi_result {
    twi_result_UNKNOWN = 0x00,
    twi_result_OK = 0x01,
    twi_result_BUFFER_OVERFLOW = 0x02,
    twi_result_ARBITRATION_LOST = 0x03,
    twi_result_BUS_ERROR = 0x04,
    twi_result_NACK_RECEIVED = 0x05,
    twi_result_FAIL = 0x06,
} twi_result_t;

typedef enum twi_status {
    twi_status_UNKNOWN = 0x00,
    twi_status_READY = 0x01,
    twi_status_BUSY = 0x02,
} twi_status_t;

typedef enum twi_error {
    twi_error_SUCCESS = 0x00,
    twi_error_MSTNULL = 0x01,
    twi_error_BADPAR = 0x02,
    twi_error_BUSNRDY = 0x03,

    twi_error_INVMCU = 0xFF,

} twi_error_t;

typedef struct twi_master {
    TWI_t *interface;
    uint8_t address;
    uint8_t tx_buffer[TWI_TRANSFER_MAX_LENGTH];
    uint8_t bytes_to_write;
    uint8_t bytes_written;
    uint8_t rx_buffer[TWI_TRANSFER_MAX_LENGTH];
    uint8_t bytes_to_read;
    uint8_t bytes_read;
    volatile twi_status_t status;
    volatile twi_result_t result;
} twi_master_t;

uint8_t twi_master_init(struct twi_master *twi_master, TWI_t *twi, uint8_t interrupt_level, uint32_t cpu_freq,
                        uint32_t twi_freq);

TWI_MASTER_BUSSTATE_t twi_master_bus_state(struct twi_master *twi_master);

uint8_t twi_master_transfer(struct twi_master *twi_master, uint8_t address, uint8_t *data, uint8_t tx_size, uint8_t rx_size);

uint8_t twi_master_write(struct twi_master *twi_master, uint8_t address, uint8_t *data, uint8_t size);

uint8_t twi_master_read(struct twi_master *twi_master, uint8_t address, uint8_t size);

uint8_t twi_master_isr_handler(struct twi_master *twi_master);

const char *twi_master_status_str(struct twi_master *twi_master);

const char *twi_master_result_str(struct twi_master *twi_master);

const char *twi_master_strerr(twi_error_t err);

#endif /* !__RU_AVRLIB_ATXMEGA_TWI_MASTER_H__ */
