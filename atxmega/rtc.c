/*
 *  rtc.c
 *
 *  Created on: 2021-02-06
 *  Author: rafau
 */

#include "rtc.h"

#include <avr/interrupt.h>

static uint32_t _src_clk_freq = 0;
static volatile uint16_t _uptime_cnt = 0;
static volatile uint16_t *_uptime = &_uptime_cnt;

void rtc_init(CLK_RTCSRC_t clk_src) {
    // Exit if already initialized
    if (_src_clk_freq != 0)
        return;

    clk_src &= (0x07 << 1);

    CLK_RTCCTRL = clk_src;
    CLK_RTCCTRL |= CLK_RTCEN_bm;

    switch (clk_src) {
    case CLK_RTCSRC_ULP_gc:
    case CLK_RTCSRC_TOSC_gc:
    case CLK_RTCSRC_RCOSC_gc:
        _src_clk_freq = 1024000;
        _uptime = &RTC_CNT;
        RTC_PER = 0xFFFF;
        RTC_CTRL = RTC_PRESCALER_DIV1024_gc;
        break;

    case CLK_RTCSRC_TOSC32_gc:
    case CLK_RTCSRC_RCOSC32_gc:
        _src_clk_freq = 32768000;
        RTC_PER = 0x0020;
        RTC_CTRL = RTC_PRESCALER_DIV1024_gc;
        RTC_INTCTRL = RTC_OVFINTLVL_HI_gc;
        break;

    case CLK_RTCSRC_EXTCLK_gc:
        _src_clk_freq = F_RTC_CLK;

        if (_src_clk_freq == 1024000) {
            _uptime = &RTC_CNT;
            RTC_PER = 0xFFFF;
            RTC_CTRL = RTC_PRESCALER_DIV1024_gc;
        } else {
            RTC_PER = (F_RTC_CLK / 1024) / 1000;
            RTC_CTRL = RTC_PRESCALER_DIV1024_gc;
            RTC_INTCTRL = RTC_OVFINTLVL_HI_gc;
        }

        break;
    }
}

uint16_t uptime(void) { return *_uptime; }

ISR(RTC_OVF_vect) { _uptime_cnt++; }