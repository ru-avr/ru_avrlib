/*
 *  usart.c
 *
 *  Created on: 2021-01-23
 *  Author: rafau
 */

#include "usart.h"

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>

uint8_t usart_gpio_init(PORT_t *const port, uint8_t rx_pin, uint8_t tx_pin) {
    if (!port || rx_pin > PIN7 || tx_pin > PIN7)
        return 1;

#if defined(__AVR_ATxmega32E5__)

    // if (port != &PORTC || port != &PORTD) return 2;
    if (rx_pin != PIN2 && rx_pin != PIN6)
        return 3;
    if (tx_pin != PIN3 && tx_pin != PIN7)
        return 4;

    // Rx - input
    port->OUT &= ~(1 << rx_pin);

    // Tx - output pulled up
    port->OUT |= (1 << tx_pin);
    port->DIR |= (1 << tx_pin);

    return 0;
#else
    return 255;
#endif
}

uint8_t usart_init(USART_t *const usart, USART_RXCINTLVL_t rxc_int_lvl, USART_TXCINTLVL_t txc_int_lvl) {
    if (!usart)
        return 1;

#if defined(__AVR_ATxmega32E5__)

    usart->CTRLA = rxc_int_lvl | txc_int_lvl;
    usart->CTRLB = USART_TXEN_bm | USART_RXEN_bm;

    usart->CTRLC &= (0x03 << 0);
    usart->CTRLC |= USART_CHSIZE_8BIT_gc;

    return 0;
#else
    return 255;
#endif
}

uint8_t usart_set_baudrate(USART_t *const usart, uint32_t cpu_freq, uint32_t baud) {
    if (!usart)
        return 1;

#if defined(__AVR_ATxmega32E5__)

    // (2^BSCALE_MIN) * 8 * (BSEL_MIN) = (2^0) * 8 * (2^0) = 8
    uint32_t max_rate = cpu_freq / 8;

    // (2^BSCALE_MAX) * 8 * (BSEL_MAX+1) = (2^7) * 8 * (2^12) = 4194304
    uint32_t min_rate = cpu_freq / 4194304;

    if (!(usart->CTRLB & USART_CLK2X_bm)) {
        max_rate /= 2;
        min_rate /= 2;
    }

    if (baud > max_rate || baud < min_rate)
        return 1;

    if (!(usart->CTRLB & USART_CLK2X_bm))
        baud *= 2;

    int32_t limit = 0x0FFF >> 4;
    int32_t ratio = cpu_freq / baud;

    int8_t exp;
    uint32_t div;

    for (exp = -7; exp < 7; exp++) {
        if (ratio < limit)
            break;

        limit <<= 1;

        if (exp < -3)
            limit |= 1;
    }

    if (exp < 0) {
        cpu_freq -= 8 * baud;

        if (exp <= -3) {
            div = ((cpu_freq << (-exp - 3)) + baud / 2) / baud;
        } else {
            baud <<= exp + 3;
            div = (cpu_freq + baud / 2) / baud;
        }

    } else {
        baud <<= exp + 3;
        div = (cpu_freq + baud / 2) / baud - 1;
    }

    usart->BAUDCTRLB = (uint8_t)(((div >> 8) & 0x0F) | (exp << 4));
    usart->BAUDCTRLA = (uint8_t)div;

    return 0;
#else
    return 255;
#endif
}

uint8_t usart_putc(USART_t *const usart, uint8_t c) {
    if (!usart)
        return 1;

#if defined(__AVR_ATxmega32E5__)

    while (!(usart->STATUS & USART_DREIF_bm))
        ;

    USARTC0.DATA = c;

    return 0;
#else
    return 255;
#endif
}

uint8_t usart_getc(USART_t *const usart) {
    if (!usart)
        return -1;

#if defined(__AVR_ATxmega32E5__)

    return usart->DATA;
#else
    return 0;
#endif
}

int32_t usart_write(USART_t *const usart, char *s, uint16_t len) {
    if (!usart)
        return -1;

    uint8_t rc = 0;
    uint16_t i = 0;

    while (i < len && !rc) {
        rc = usart_putc(usart, s[i++]);
    }

    return (int32_t)i;
}

int32_t usart_puts(USART_t *const usart, char *s) {
    if (!usart)
        return -1;

    uint8_t rc;
    int32_t bytes_send = 0;

    while (*s) {
        rc = usart_putc(usart, *s++);

        if (rc)
            break;

        ++bytes_send;
    }

    return bytes_send;
}

int32_t usart_printf(USART_t *const usart, const char *format, ...) {
    static char buffer[XMEGA_USART_PRINTF_BUFFER_SIZE] = {0};

    if (!usart)
        return -1;

    va_list args;
    int32_t rc;

    va_start(args, format);
    rc = vsnprintf(buffer, XMEGA_USART_PRINTF_BUFFER_SIZE, format, args);
    va_end(args);

    if (rc <= 0)
        return rc;

    return usart_write(usart, buffer, rc);
}
