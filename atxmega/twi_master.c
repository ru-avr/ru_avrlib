/*
 *  twi_master.c
 *
 *  Created on: 2021-01-23
 *  Author: rafau
 */

#include "twi_master.h"

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

uint8_t twi_master_init(struct twi_master *twi_master, TWI_t *twi, uint8_t interrupt_level, uint32_t cpu_freq,
                        uint32_t twi_freq) {
    if (!twi_master) return twi_error_MSTNULL;

    if (!twi) return twi_error_BADPAR;

    memset(twi_master, 0, sizeof(struct twi_master));

    twi_master->interface = twi;

    interrupt_level &= (0x03 << 6);

#if defined(__AVR_ATxmega32E5__)

    twi_master->interface->MASTER.CTRLA = interrupt_level | TWI_MASTER_WIEN_bm | TWI_MASTER_RIEN_bm | TWI_MASTER_ENABLE_bm;

    // BAUD = (cpu_freq/(2*twi_freq))-5
    // twi_master->interface->MASTER.BAUD = 0x05;
    twi_master->interface->MASTER.BAUD = ((cpu_freq / (2 * twi_freq)) - 5);

    twi_master->interface->MASTER.STATUS = TWI_MASTER_BUSSTATE_IDLE_gc;

    twi_master->status = twi_status_READY;

    return twi_error_SUCCESS;
#else
    return twi_error_INVMCU;
#endif
}

TWI_MASTER_BUSSTATE_t twi_master_bus_state(struct twi_master *twi_master) {
    if (!twi_master) return TWI_MASTER_BUSSTATE_UNKNOWN_gc;

#if defined(__AVR_ATxmega32E5__)

    TWI_MASTER_BUSSTATE_t bus_state = twi_master->interface->MASTER.STATUS & TWI_MASTER_BUSSTATE_gm;

    return bus_state;
#else
    return TWI_MASTER_BUSSTATE_UNKNOWN_gc;
#endif
}

uint8_t twi_master_ready(struct twi_master *twi_master) {
    if (!twi_master) return 0;

#if defined(__AVR_ATxmega32E5__)

    uint8_t ready = !!(twi_master->status & twi_status_READY);

    return ready;
#else
    return 0;
#endif
}

uint8_t twi_master_transfer(struct twi_master *twi_master, uint8_t address, uint8_t *data, uint8_t tx_size, uint8_t rx_size) {
    if (!twi_master) return twi_error_MSTNULL;

    if (tx_size > TWI_TRANSFER_MAX_LENGTH || rx_size > TWI_TRANSFER_MAX_LENGTH) return twi_error_BADPAR;

    if (twi_master->status != twi_status_READY) return twi_error_BUSNRDY;

    twi_master->status = twi_status_BUSY;
    twi_master->result = twi_result_UNKNOWN;
    twi_master->address = address << 1;

    memcpy(twi_master->tx_buffer, data, tx_size);

    twi_master->bytes_to_write = tx_size;
    twi_master->bytes_to_read = rx_size;
    twi_master->bytes_written = 0;
    twi_master->bytes_read = 0;

    if (twi_master->bytes_to_write > 0)
        twi_master->interface->MASTER.ADDR = twi_master->address;
    else if (twi_master->bytes_to_read > 0)
        twi_master->interface->MASTER.ADDR = twi_master->address | 0x01;

    return 0;
}

uint8_t twi_master_write(struct twi_master *twi_master, uint8_t address, uint8_t *data, uint8_t size) {
    if (!twi_master) return twi_error_MSTNULL;

    return twi_master_transfer(twi_master, address, data, size, 0);
}

uint8_t twi_master_read(struct twi_master *twi_master, uint8_t address, uint8_t size) {
    if (!twi_master) return twi_error_MSTNULL;

    return twi_master_transfer(twi_master, address, 0, 0, size);
}

static void _on_arbitration_lost(struct twi_master *twi_master) {
    TWI_t *twi = twi_master->interface;

    if (twi->MASTER.STATUS & TWI_MASTER_BUSERR_bm)
        twi_master->result = twi_result_BUS_ERROR;
    else
        twi_master->result = twi_result_ARBITRATION_LOST;

    twi->MASTER.STATUS = twi->MASTER.STATUS | TWI_MASTER_ARBLOST_bm;

    twi_master->status = twi_status_READY;
}

static void _on_transaction_finished(struct twi_master *twi_master, twi_result_t result) {
    twi_master->result = result;
    twi_master->status = twi_status_READY;
}

static void _on_write_complete(struct twi_master *twi_master) {
    uint8_t bytes_to_write = twi_master->bytes_to_write;
    uint8_t bytes_to_read = twi_master->bytes_to_read;
    TWI_t *twi = twi_master->interface;

    if (twi->MASTER.STATUS & TWI_MASTER_RXACK_bm) {
        twi->MASTER.CTRLC = TWI_MASTER_CMD_STOP_gc;
        twi_master->result = twi_result_NACK_RECEIVED;
        twi_master->status = twi_status_READY;

    } else if (twi_master->bytes_written < bytes_to_write) {
        twi->MASTER.DATA = twi_master->tx_buffer[twi_master->bytes_written++];

    } else if (twi_master->bytes_read < bytes_to_read) {
        twi->MASTER.ADDR = twi_master->address | 0x01;

    } else {
        twi->MASTER.CTRLC = TWI_MASTER_CMD_STOP_gc;
        _on_transaction_finished(twi_master, twi_result_OK);
    }
}

static void _on_read_complete(struct twi_master *twi_master) {
    TWI_t *twi = twi_master->interface;

    if (twi_master->bytes_read < TWI_TRANSFER_MAX_LENGTH) {
        twi_master->rx_buffer[twi_master->bytes_read] = twi->MASTER.DATA;
        twi_master->bytes_read++;
    } else {
        twi->MASTER.CTRLC = TWI_MASTER_CMD_STOP_gc;
        _on_transaction_finished(twi_master, twi_result_BUFFER_OVERFLOW);
    }

    if (twi_master->bytes_read < twi_master->bytes_to_read) {
        twi->MASTER.CTRLC = TWI_MASTER_CMD_RECVTRANS_gc;
    } else {
        twi->MASTER.CTRLC = TWI_MASTER_ACKACT_bm | TWI_MASTER_CMD_STOP_gc;
        _on_transaction_finished(twi_master, twi_result_OK);
    }
}

uint8_t twi_master_isr_handler(struct twi_master *twi_master) {
    if (!twi_master) return twi_error_MSTNULL;

    TWI_t *twi = twi_master->interface;

    if ((twi->MASTER.STATUS & TWI_MASTER_ARBLOST_bm) || (twi->MASTER.STATUS & TWI_MASTER_BUSERR_bm))
        _on_arbitration_lost(twi_master);

    else if (twi->MASTER.STATUS & TWI_MASTER_WIF_bm)
        _on_write_complete(twi_master);

    else if (twi->MASTER.STATUS & TWI_MASTER_RIF_bm)
        _on_read_complete(twi_master);

    else
        _on_transaction_finished(twi_master, twi_result_FAIL);

    return 0;
}

const char *twi_master_status_str(struct twi_master *twi_master) {
    if (twi_master) {
        switch (twi_master->status) {
            case twi_status_UNKNOWN: return "Unknown";
            case twi_status_READY: return "Ready";
            case twi_status_BUSY: return "Busy";
            default: break;
        }
    }

    return "undefined";
}

const char *twi_master_result_str(struct twi_master *twi_master) {
    if (twi_master) {
        switch (twi_master->result) {
            case twi_result_UNKNOWN: return "Unknown";
            case twi_result_OK: return "Ok";
            case twi_result_BUFFER_OVERFLOW: return "Buffer Overflow";
            case twi_result_ARBITRATION_LOST: return "Arbitration Lost";
            case twi_result_BUS_ERROR: return "Bus Error";
            case twi_result_NACK_RECEIVED: return "NACK Received";
            case twi_result_FAIL: return "Fail";
            default: break;
        }
    }

    return "undefined";
}

const char *twi_master_strerr(twi_error_t err) {
    switch (err) {
        case twi_error_SUCCESS: return "Success";
        case twi_error_MSTNULL: return "struct twi_master is null";
        case twi_error_BADPAR: return "Invalid parameter";
        case twi_error_BUSNRDY: return "Bus busy";

        case twi_error_INVMCU: return "MCU not supported";
        default: break;
    }

    return "undefined";
}