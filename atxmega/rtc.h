/*
 *  rtc.h
 *
 *  Created on: 2021-02-06
 *  Author: rafau
 */

#ifndef __RU_AVRLIB_ATXMEGA_RTC_H__
#define __RU_AVRLIB_ATXMEGA_RTC_H__

#include <avr/io.h>

void rtc_init(CLK_RTCSRC_t clk_src);

/**
 * Returns uptime since rtc initialization in seconds
 */
uint16_t uptime(void);

#endif /* !__RU_AVRLIB_ATXMEGA_RTC_H__ */
