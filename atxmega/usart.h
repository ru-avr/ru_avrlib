/*
 *  usart.h
 *
 *  Created on: 2021-01-23
 *  Author: rafau
 */

#ifndef __RU_AVRLIB_ATXMEGA_USART_H__
#define __RU_AVRLIB_ATXMEGA_USART_H__

#include <avr/io.h>
#include <stdarg.h>
#include <stdint.h>

#ifndef XMEGA_USART_PRINTF_BUFFER_SIZE
#pragma message("XMEGA_USART_PRINTF_BUFFER_SIZE not defined, default value used. XMEGA_USART_PRINTF_BUFFER_SIZE=256")
#define XMEGA_USART_PRINTF_BUFFER_SIZE 256
#endif // !XMEGA_USART_PRINTF_BUFFER_SIZE

/**
 * Initialize gpio pins for uart
 */
uint8_t usart_gpio_init(PORT_t *const port, uint8_t rx_pin, uint8_t tx_pin);

/**
 * Initialize usart, no parity, 8 data bits, 1 stop bit
 */
uint8_t usart_init(USART_t *const usart, USART_RXCINTLVL_t rxc_int_lvl, USART_TXCINTLVL_t txc_int_lvl);

/**
 * Set usart baudrate
 */
uint8_t usart_set_baudrate(USART_t *const usart, uint32_t cpu_freq, uint32_t baud);

/**
 * Blocking send byte to uart
 */
uint8_t usart_putc(USART_t *const usart, uint8_t c);

/**
 * Blocking receive byte from uart
 */
uint8_t usart_getc(USART_t *const usart);

/**
 * Blocking write bytes to uart
 */
int32_t usart_write(USART_t *const usart, char *s, uint16_t len);

/**
 * Blocking send string to uart
 */
int32_t usart_puts(USART_t *const usart, char *s);

/**
 * Blocking printf to uart
 */
int32_t usart_printf(USART_t *const usart, const char *format, ...);

#endif /* !__RU_AVRLIB_ATXMEGA_USART_H__ */
